#exemple liste de dictionnaires

liste=[ {'ville':'Calais','temps':'magnifique'},{'ville':'Paris','temps':'pluie'},{'ville':'Vegas','temps':'pluie'},{'ville':'Granville','temps':'ensoleille'}]
print("affichage bloc\n")
print(liste)

print("\n")
print("---- \n")
print("affichage par ligne\n")
for i in range(len(liste)):
    print(liste[i])


print("\nAffichage element 2\n")
print(liste[2].items())

print("\nAffichage temps de element 2\n")
for cle,valeur in liste[2].items():
    if (cle=="temps"):
        print(cle, ":",valeur)

print("\nAffichage de tous les temps\n")
for i in range(len(liste)):
    print(liste[i]['temps'])
