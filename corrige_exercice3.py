# ceci est un commentaire

#premier exemple de fonction
def maFonction(a):
    return a*3

#premier exemple de fonction identique
def maFonction2(a):
    res=a*3
    return res
    
maVariable=5
print(maFonction(maVariable))
print(maFonction2(maVariable))

# print(res) : entraine une erreur car la variable n'a d'existence que dans le bloc de la fonction

#deuxieme exemple

def afficheListe(liste):
    print("Affichage liste par element")
    for i in range(len(liste)):
        print(liste[i])
 
liste1=[6,10,1,100]
afficheListe(liste1)
 
liste2=[5.1,1,7.3,100,0,4]
afficheListe(liste2)

# enoncé : ecrire une fonction qui prend en argument une liste et renvoie la liste dont les elements ont été multiplié par leur position dans la liste.
# correction de l'énoncé 
def enonceExo(liste):
    N=len(liste)
    for i in range(N):
        liste[i]=liste[i]*i
    return liste

maListe=[1,2,3,10]    
resListe=enonceExo(maListe)
print(maListe)
print(resListe)
# on remarque que ma liste est modifié

# enoncé 2 : identique mais la liste initiale non modifiée
# ecrire une fonction qui prend en argument une liste et renvoie la liste dont les elements ont été multiplié par leur position dans la liste.
# correction de l'énoncé 2 
def enonceExo2(liste):
    N=len(liste)
    listeRes=[]
    for i in range(N):
        listeRes.append(liste[i]*i)
    return listeRes

maListe=[1,2,3,10]  
resListe2=enonceExo2(maListe)
print(maListe)
print(resListe2)

